import { Component, OnInit } from '@angular/core';
import { FormControl, Validators , FormGroup, FormBuilder } from '@angular/forms';
import { User } from '../models/user';
import { UserResponse } from "../models/user.response";
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import {MdSnackBar} from '@angular/material';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signUpForm: FormGroup
  user: User = new User("", "", "", "", "")
  imgLoading: boolean
  img = ''
  itemId: any
  answer: string
  validationState: boolean
  headers: HttpHeaders
  authKey: any
  /**
   * se inyecta una instancia al crear el componente
   * @param formBuilder instancia inyectada
   */
  constructor(formBuilder: FormBuilder, private http: HttpClient, private snackBar: MdSnackBar) { 
    // Se construyen los FormControls
    this.signUpForm = formBuilder.group({
      'nameFormControl': ['', Validators.required],
      'lastNameFormControl': ['', Validators.required],
      'usernameFormControl': ['', Validators.compose([Validators.required])],
      'emailFormControl': ['', Validators.required],
      'passwordFormControl': ['', Validators.required],
      'confirmPasswordFormControl': ['', Validators.required],
      'captchaFormControl': ['', Validators.required]
    })
  }

  /**
   * Validación personalizada para un formControl en específico
   * @param control 
   */
  customValidation(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match()) { 
      return {invalidPattern: true};
    }
  }
  /**
   * Método encargado de manejar el evento ngSubmit del formulario
   * Se hacen las llamadas http correspondientes (comprobación del captcha e inserción del usuario)
   * @param formValue valor del formulario
   */
  onSubmit(formValue: Object){
    // console.log(this.user)
    let captchaURL = `http://bx-platform-api.azurewebsites.net/captcha/check?itemId=${this.itemId}&answer=${this.answer}`
    let userPostURL = 'http://bx-platform-api.azurewebsites.net/user/signup'
    //let headers = new Headers({ 'Content-Type': 'application/json' })

    console.log(this.user)
    //headers.append('Authorization', this.authKey );
    this.http.post(captchaURL, {})
      .subscribe(
        res => {
          console.log('SUCCESS')
          this.validationState = res['validation']
          this.authKey = res['authKey']
          console.log('AUTH KEY' +  this.authKey)
          if(this.validationState){ 
            this.http.post(userPostURL, this.user, {
              headers: new HttpHeaders().set('Authorization', `${this.authKey}`),
              })
              .subscribe(
                res => {
                  console.log(res)
                  this.openSnackBar('Usuario ingresado con éxito', 'Ok')
                },
                err=> {
                  console.log(`Error ocurred: ${JSON.stringify(err)}`)
                }
              )
          }
          else{
            console.log(this.validationState)
            console.log("Imposible insertar usuario debido a un error ocurrido durante la verificación del captcha")
          }

        },
        err => {
          console.log("ha ocurrido un error con la verificación del captcha");
        }
      )
  }

  /**
   * Se ejecuta la llamada al servidor para obtener el captcha
   */
  ngOnInit() {
    this.rechargeTheCaptcha()
  }

  rechargeTheCaptcha(){
    this.http.get<UserResponse>('http://bx-platform-api.azurewebsites.net/captcha/get')
    .subscribe(
      data => {
        this.imgLoading = true
        this.img = data.reference
        this.itemId = data.itemId
        console.log(data.itemId)
        console.log(data.reference)
      },
      (err: HttpErrorResponse) => {
        console.log(err)
      }
    )
  }

  /**
   * 
   * @param message - mensaje a desplegar
   * @param action - botón a desplegar
   */
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
