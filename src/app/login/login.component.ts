import { Component, OnInit } from '@angular/core';
import {
  FormBuilder, FormGroup, Validators,
  AbstractControl
} from '@angular/forms';
import { HttpClient } from "@angular/common/http";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  myForm: FormGroup
  loggedUser: any = {}
  constructor(fb: FormBuilder, private http: HttpClient) {
    this.myForm = fb.group({
      'username': ['', Validators.required],
      'password': ['', Validators.required]
    })
  }

  /**
   * Metodo encargado de manejar el envío de datos provenientes del formulario
   * @param value - valor del formulario
   */
  onSubmit(value: string){
    console.log(value)
  }

  ngOnInit() {
  }

  login(username, password) {
    this.loggedUser.username = username
    this.loggedUser.password = password 
    let userLoginURL = 'http://bx-platform-api.azurewebsites.net/user/login'
    this.http.post(userLoginURL, this.loggedUser)
      .subscribe(
        res => {
          console.log(JSON.stringify(res))
        },
        err => {
          console.log(JSON.stringify(err))
        }
      )
  }

}
