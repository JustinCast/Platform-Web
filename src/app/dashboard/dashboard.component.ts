import { Component, OnInit } from '@angular/core';
import {MenuModule,MenuItem} from 'primeng/primeng';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  items: MenuItem[];
  constructor() { 
    this.items = [{
      label: 'File',
    }]
  } 

  ngOnInit() {
  }

}
