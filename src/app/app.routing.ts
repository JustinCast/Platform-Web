import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ErrorHandleComponent } from './error-handle/error-handle.component';
import { Routes } from '@angular/router';
export const ROUTES : Routes= [
    {
        path: '', redirectTo: 'login', pathMatch: 'full'
    },
    {
        path: 'login', component: LoginComponent
    },
    {
        path: 'signup', component: SignupComponent
    },
    {
        path: 'dashboard', component: DashboardComponent
    },
    //componente en caso de error
    {
        path: '**', component: ErrorHandleComponent
    }
]