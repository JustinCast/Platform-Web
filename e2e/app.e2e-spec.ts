import { Platform.WebPage } from './app.po';

describe('platform.web App', () => {
  let page: Platform.WebPage;

  beforeEach(() => {
    page = new Platform.WebPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
